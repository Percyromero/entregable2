package mis.pruebas.springbootentregable2.servicio.repos;

import mis.pruebas.springbootentregable2.modelo.Campania;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioCampanias extends MongoRepository<Campania, String> {
}
