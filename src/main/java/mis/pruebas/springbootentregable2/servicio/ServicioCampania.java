package mis.pruebas.springbootentregable2.servicio;

import mis.pruebas.springbootentregable2.modelo.Campania;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ServicioCampania {

    public String agregar(Campania c);
    public List<Campania> obtenerCampanias();
    public Campania obtenerCampania(String id);
    public void reemplazarCampania(String id, Campania c);
    public void borrarCampania(String id);

}
