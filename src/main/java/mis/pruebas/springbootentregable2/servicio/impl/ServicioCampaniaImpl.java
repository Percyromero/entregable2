package mis.pruebas.springbootentregable2.servicio.impl;

import mis.pruebas.springbootentregable2.modelo.Campania;
import mis.pruebas.springbootentregable2.servicio.ServicioCampania;
import mis.pruebas.springbootentregable2.servicio.repos.RepositorioCampanias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioCampaniaImpl implements ServicioCampania {

    @Autowired
    RepositorioCampanias repositorioCampanias;

    @Override
    public String agregar(Campania c) {
        this.repositorioCampanias.insert(c);
        return c.id;
    }

    @Override
    public List<Campania> obtenerCampanias() {
        return this.repositorioCampanias.findAll();
    }

    @Override
    public Campania obtenerCampania(String id) {
        final Optional<Campania> c = this.repositorioCampanias.findById(id);
        return c.isPresent() ? c.get() : null;
    }

    @Override
    public void reemplazarCampania(String id, Campania c) {
        c.id = id;
        this.repositorioCampanias.save(c);
    }

    @Override
    public void borrarCampania(String id) {
        this.repositorioCampanias.deleteById(id);
    }
}
