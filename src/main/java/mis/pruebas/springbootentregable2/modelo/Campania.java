package mis.pruebas.springbootentregable2.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Campania {

    @JsonIgnore
    @Id
    public String id;
    public String nombre;
    public String descripcion;
    public String fechaInicio;
    public String fechaFin;
    public String estado;

    public List<String> idProveedores = new ArrayList<>();
}
