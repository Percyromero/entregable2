package mis.pruebas.springbootentregable2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEntregable2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEntregable2Application.class, args);
	}

}
