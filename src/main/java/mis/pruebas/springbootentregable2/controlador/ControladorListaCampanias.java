package mis.pruebas.springbootentregable2.controlador;

import mis.pruebas.springbootentregable2.modelo.Campania;
import mis.pruebas.springbootentregable2.servicio.ServicioCampania;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("${api.v1}/campanias")
public class ControladorListaCampanias {

    @Autowired
    ServicioCampania servicioCampania;

    //POST
    @PostMapping
    public void agregarCampania(@RequestBody Campania c){

        this.servicioCampania.agregar(c);
    }

    //GET
    @GetMapping
    public List<Campania> obtenerCampanias(){
        return this.servicioCampania.obtenerCampanias();
    }

}
